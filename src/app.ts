import http from 'http';
import express, { Request, Response, NextFunction } from 'express';
import HttpStatus from 'http-status-codes';
import createHttpError from 'http-errors';

import mainController from './controllers/main';
import apiController from './controllers/api';

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', mainController);
app.use('/api', apiController);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createHttpError(HttpStatus.NOT_FOUND));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || HttpStatus.INTERNAL_SERVER_ERROR);
  res.send({status: err.status, message: err.message});
});

app.set('port', port);

const server = http.createServer(app);

server.listen(port);
server.on('error', onError);

/** Event listener for HTTP server "error" event */
function onError(error: any) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}
