import { Router } from 'express';
import createError from 'http-errors';
import HttpStatus from 'http-status-codes';
import employeesController from './api/v1/employees';
import managersController from './api/v1/managers';

const router = Router();

router.get('/', function(req, res, next) {
  res.send('Yay! It works!');
});

router.use('/v1/employees', employeesController);
router.use('/v1/managers', managersController);

router.use(function(req, res, next) {
  next(createError(HttpStatus.NOT_IMPLEMENTED));
});

export default router;
