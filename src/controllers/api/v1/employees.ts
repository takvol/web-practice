import express from 'express';
import HttpStatus from 'http-status-codes';
import createHttpError from 'http-errors';
import { FakeDb } from '../../../FakeDb';
import { Developer } from '../../../models/Developer';
import { Designer } from '../../../models/Designer';
import { MapEmployee } from './Mapper';

const router = express.Router();

/* GET employees listing. */
router.get('/', function(req, res, next) {
  let employees = FakeDb.employees.map(employee => MapEmployee(employee));
  res.send(employees);
});

router.post('/', function (req, res, next) {
  let employee;

  if (req.body.type == "developer") {
    employee = new Developer(req.body);
  } else if (req.body.type == "designer") {
    employee = new Designer(req.body);
  } else {
    next(createHttpError(HttpStatus.BAD_REQUEST, 'type not recognized'));
    return;
  }

  FakeDb.employees.push(employee);

  res.send({
    id: FakeDb.employees.indexOf(employee)
  });
});

router.get('/:id', function(req, res, next) {
  let employeeRecord = FakeDb.employees[Number(req.params.id)];

  if (employeeRecord) {
    let employee = MapEmployee(employeeRecord, true);
    res.json(employee);
  } else {
    next(createHttpError(HttpStatus.NOT_FOUND, 'employee not found'));
  }
});

export default router;
