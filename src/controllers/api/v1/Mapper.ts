import { Manager } from '../../../models/Manager';
import { Employee } from '../../../models/Employee';
import { Designer } from '../../../models/Designer';
import { FakeDb } from '../../../FakeDb';

interface MappedObject {
  id: null | number,
  type: string,
  firstName: Employee["firstName"],
  lastName: Employee["lastName"],
  salary: Employee["salary"],
  experience: Employee["experience"],
  effCoeff?: Designer["effCoeff"],
  manager_id?: number
}

function MapEmployee(employee: Employee, calculateSalary:boolean = false): MappedObject {
  let mapped: MappedObject = {
    id: null,
    type: employee.constructor.name.toLowerCase(),
    firstName: employee.firstName,
    lastName: employee.lastName,
    salary: calculateSalary ? employee.salaryValue : employee.salary,
    experience: employee.experience,
  }
  let collection = employee instanceof Manager ? FakeDb.managers : FakeDb.employees;
  let index = collection.indexOf(employee);

  if (index != -1) {
    mapped.id = index;
  }

  if (employee instanceof Designer) {
    mapped.effCoeff = employee.effCoeff;
  }

  if (employee.manager) {
    let index = FakeDb.managers.indexOf(employee.manager);

    if (index != -1) {
      mapped.manager_id = index;
    }
  }

  return mapped;
}

export { MapEmployee };
