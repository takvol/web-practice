import express from 'express';
import HttpStatus from 'http-status-codes';
import createHttpError from 'http-errors';
import { FakeDb } from '../../../FakeDb';
import { Manager } from '../../../models/Manager';
import { MapEmployee } from './Mapper';

const router = express.Router();

/* GET managers listing. */
router.get('/', function(req, res, next) {
  let managers = FakeDb.managers.map(manager => MapEmployee(manager));
  res.send(managers);
});

router.post('/', function (req, res) {
  let manager = new Manager(req.body);

  FakeDb.managers.push(manager);

  res.send({
    id: FakeDb.managers.indexOf(manager)
  });
});

router.get('/:id', function(req, res, next) {
  let managerRecord = FakeDb.managers[Number(req.params.id)];

  if (managerRecord) {
    let manager = MapEmployee(managerRecord, true);
    res.json(manager);
  } else {
    next(createHttpError(HttpStatus.NOT_FOUND, 'manager not found'));
  }
});

router.get('/:id/team', function(req, res, next) {
  let managerRecord = FakeDb.managers[Number(req.params.id)];

  if (managerRecord) {
    let team = managerRecord.team.map(employee => MapEmployee(employee));
    res.json(team);
  } else {
    next(createHttpError(HttpStatus.NOT_FOUND, 'manager not found'));
  }
});

router.post('/:id/team', function (req, res, next) {
  let manager = FakeDb.managers[Number(req.params.id)];

  if (manager) {
    let employee = FakeDb.employees[Number(req.body.employee_id)];

    if (employee) {
      manager.addToTeam(employee);
      res.end();
    } else {
      next(createHttpError(HttpStatus.NOT_FOUND, 'employee not found'));
    }
  } else {
    next(createHttpError(HttpStatus.NOT_FOUND, 'manager not found'));
  }
});

export default router;
