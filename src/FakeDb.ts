import { Department } from './models/Department';
import { Manager } from './models/Manager';
import { Developer } from './models/Developer';
import { Designer } from './models/Designer';

let ShaoKhan = new Manager({firstName: "Shao", lastName: "Khan", salary: 3000, experience: 3});
let Scrorpion = new Developer({firstName: "Hanzo", lastName: "Hasashi", salary: 1000, experience: 3});
let SubZero = new Developer({firstName: "Kuai", lastName: "Liang", salary: 1000, experience: 3});

let Raiden = new Manager({firstName: "Raiden", lastName: "Thunder God", salary: 2000, experience: 6});
let Johnny = new Designer({firstName: "Johnny", lastName: "Cage", salary: 3000, experience: 2, effCoeff: 0.8});
let Jax = new Developer({firstName: "Jackson", lastName: "Briggs", salary: 1500, experience: 2});

ShaoKhan.addToTeam(Scrorpion);
ShaoKhan.addToTeam(SubZero);

Raiden.addToTeam(Johnny);
Raiden.addToTeam(Jax);

let employees = [Scrorpion, SubZero, Johnny, Jax];
let managers = [ShaoKhan, Raiden];
let FakeDb = {employees, managers};

export { FakeDb };
