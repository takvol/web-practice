import { Employee, EmployeeParameters } from './Employee';

class Developer extends Employee {
    constructor(params: EmployeeParameters) {
        super(params);
        this.isTeamPlayer = true;
    }
}

export { Developer };
