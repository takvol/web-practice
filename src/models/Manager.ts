import { Employee, EmployeeParameters } from './Employee';
import { Developer } from './Developer';
import { Department } from './Department';

class Manager extends Employee {
  private _team: Set<Employee>;
  public department: Department;

  constructor(params: EmployeeParameters) {
    super(params);
    this._team = new Set();
    this.department = null;
  }

  get salaryValue():number {
    let salaryValue = super.salaryValue;
    let devsInTeam = this.team.filter((employee) => {
      return employee instanceof Developer;
    }).length;
    let isBonus = devsInTeam > this._team.size / 2;

    if (this._team.size > 10) {
      salaryValue += 300;
    } else if (this._team.size > 5) {
      salaryValue += 200;
    }

    if (isBonus) {
      salaryValue *= 1.1;
    }

    return salaryValue;
  }

  get team():Array<Employee> {
    return Array.from(this._team);
  }

  addToTeam(employee: Employee):void {
    if (!employee.isTeamPlayer) {
      throw new Error(`Can't add ${employee.constructor.name} as a team member. Employee is not a team player.`);
    }

    if (employee.manager && employee.manager !== this) {
      employee.manager.removeFromTeam(employee);
    }

    this._team.add(employee);
    employee.manager = this;
  }

  removeFromTeam(employee: Employee):void {
    if (employee.manager !== this) {
      throw new Error('Employee is not managed by this manager');
    }
    this._team.delete(employee);
    employee.manager = null;
  }
}

export { Manager };
