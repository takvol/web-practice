import { Manager } from './Manager';
import { Employee } from './Employee';

class Department {
    private _managers: Set<Manager>;

    constructor() {
        this._managers = new Set();
    }

    get managers() {
        return Array.from(this._managers);
    }

    get employees () {
        let employees: Employee[] = [];
        
        this.managers.forEach((manager) => {
            if (manager.team) {
                employees.push(...manager.team);
            }
        });

        return employees;
    }
    
    addManager(manager: Manager) {
        if (manager.department && manager.department !== this) {
            manager.department.removeManager(manager);
        }
        this._managers.add(manager);
        manager.department = this;
    }

    removeManager(manager: Manager) {
        if (manager.department === this) {
            this._managers.delete(manager);
            manager.department = null;
        }
    }

    giveSalary() {
        [...this.managers, ...this.employees].forEach(employee => {
            console.log(`${employee.firstName} ${employee.lastName}: got salary: ${employee.salaryValue}`);
        });
    }
}

export { Department };
