import { Manager } from './Manager';

interface EmployeeParameters {
  firstName: string,
  lastName: string,
  salary: number,
  experience: number
}

class Employee {
  public isTeamPlayer: boolean;
  public manager: Manager;

  constructor(params: EmployeeParameters) {
    this.firstName = params.firstName;
    this.lastName = params.lastName;
    this.salary = params.salary;
    this.experience = params.experience;
    this.isTeamPlayer = false;
    this.manager = undefined;
  }

  get salaryValue():number {
    let salaryValue = this.salary;

    if (this.experience > 5) {
      salaryValue = salaryValue * 1.2 + 500;
    } else if (this.experience > 2) {
      salaryValue += 200;
    }

    return salaryValue;
  }

  toString() {
    return `${this.firstName} ${this.lastName}, manager: ${this.manager ? this.manager.lastName : 'none'}, experience: ${this.experience}`;
  }
}

// Declaring an interface with the same name as a class is how you add members to the class without implementing them
// https://www.typescriptlang.org/docs/handbook/declaration-merging.html#merging-interfaces
// https://github.com/Microsoft/TypeScript/issues/9699#issuecomment-232477522
interface Employee extends EmployeeParameters {}

export { Employee, EmployeeParameters };
