import { Employee, EmployeeParameters } from './Employee';

interface DesignerParameters extends EmployeeParameters {
    effCoeff: number
}

class Designer extends Employee {
    private _effCoeff: number;

    constructor(params: DesignerParameters) {
        super(params);
        this.isTeamPlayer = true;
        Designer._validateEffCoeff(params.effCoeff);
        this._effCoeff = params.effCoeff;
    }

    private static _validateEffCoeff (effCoeff: number):void {
        if (effCoeff > 1 || effCoeff <= 0) {
            throw new Error('Invalid effCoeff');
        }
    }

    get salaryValue ():number {
        return super.salaryValue * this.effCoeff;
    }

    get effCoeff ():number {
        return this._effCoeff;
    }

    set effCoeff (effCoeff: number) {
        Designer._validateEffCoeff(effCoeff);
        this._effCoeff = effCoeff;
    }
}

export { Designer };
