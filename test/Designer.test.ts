import { Designer } from '../src/models/Designer';
import { Employee } from '../src/models/Employee';

describe("Designer", function() {
  let employee: Designer;
  let employeeParams: any;
  
    beforeEach(function() {
      employeeParams = {firstName: 'Bran', lastName: 'Stark', salary: 1000, experience: 3, effCoeff: 0.5};
      employee = new Designer(employeeParams);
    });

    test("can't create with invalid effCoeff", function () {
      let newParams = employeeParams;
      newParams.effCoeff = 0;
      expect(() => new Designer(newParams)).toThrowError();
    });

    test("can't set effCoef <=0", function () {
      expect(() => {employee.effCoeff = -1}).toThrowError();
    });

    test("can't set effCoef > 1", function () {
      expect(() => {employee.effCoeff = 2}).toThrowError();
    });

    test("is a team player", function () {
      expect(employee.isTeamPlayer).toBe(true);
    });

    test("salaryValue is correct", function() {
      let doppelganger = new Employee(employeeParams);
      expect(employee.salaryValue).toEqual(doppelganger.salaryValue * employee.effCoeff);
    });
  });
  
