import { Manager } from '../src/models/Manager';
import { Developer } from '../src/models/Developer';
import { Designer } from '../src/models/Designer';
import { Employee } from '../src/models/Employee';

describe("Manager", function () {
  let manager: Manager, managerParams: any;

  beforeEach(function () {
    managerParams = { firstName: 'Joffrey', lastName: 'Baratheon', salary: 1500, experience: 1 };
    manager = new Manager(managerParams);
  });

  test("is not a team player", function () {
    expect(manager.isTeamPlayer).toBe(false);
  });

  test("add to team is ok", function () {
    let dev = new Developer({ firstName: 'Sandor', lastName: 'Clegane', salary: 800, experience: 5 });

    manager.addToTeam(dev);
    expect(manager.team.includes(dev)).toBe(true);
  });

  test("can't add not a team player", function () {
    let employee = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });
    expect(() => manager.addToTeam(employee)).toThrowError();
  });

  test("remove from team is ok", function () {
    let dev = new Developer({ firstName: 'Sandor', lastName: 'Clegane', salary: 800, experience: 5 });

    manager.addToTeam(dev);
    manager.removeFromTeam(dev);
    expect(manager.team.includes(dev)).toBe(false);
  });

  describe("salaryValue is correct", function () {
    test("for team > 10", function () {
      let doppelganger = new Employee(managerParams);

      for (let i = 0; i < 11; i++) {
        manager.addToTeam(new Designer({ firstName: 'Golden', lastName: 'Sword', salary: 600, experience: 2, effCoeff: 0.5}));
      }

      expect(manager.salaryValue).toEqual(doppelganger.salaryValue + 300);
    });

    test("for team > 5", function () {
      let doppelganger = new Employee(managerParams);

      for (let i = 0; i < 8; i++) {
        manager.addToTeam(new Designer({ firstName: 'Golden', lastName: 'Sword', salary: 600, experience: 2, effCoeff: 0.5 }));
      }

      expect(manager.salaryValue).toEqual(doppelganger.salaryValue + 200);
    });

    test("for devs > team / 2", function () {
      let doppelganger = new Employee(managerParams);

      manager.addToTeam(new Developer({ firstName: 'Golden', lastName: 'Sword', salary: 600, experience: 2 }));
      expect(manager.salaryValue).toEqual(doppelganger.salaryValue * 1.1);
    });
  });
});
