import { Developer } from '../src/models/Developer';

describe("Developer", function () {
  let employee: Developer;
  let employeeParams: any;

  beforeEach(function () {
    employeeParams = { firstName: 'Bran', lastName: 'Stark', salary: 1000, experience: 3 };
    employee = new Developer(employeeParams);
  });

  test("is a team player", function () {
    expect(employee.isTeamPlayer).toBe(true);
  });
});
