import { Employee } from '../src/models/Employee';

describe("Employee", function () {
  let employee: Employee;
  let employeeParams: any;

  beforeEach(function () {
    employeeParams = { firstName: 'Bran', lastName: 'Stark', salary: 1000, experience: 3 };
    employee = new Employee(employeeParams);
  });

  test("is not a team player", function () {
    expect(employee.isTeamPlayer).toBe(false);
  });

  test("salaryValue is correct for exp > 2", function () {
    expect(employee.salaryValue).toEqual(employeeParams.salary + 200);
  });

  test("salaryValue is correct for exp > 5", function () {
    employee.experience = 6;
    expect(employee.salaryValue).toEqual(employeeParams.salary * 1.2 + 500);
  });

  test("toString is correct", function () {
    employee.experience = 6;
    expect(employee.toString()).toEqual(`${employeeParams.firstName} ${employeeParams.lastName}, manager: none, experience: 6`);
  });
});
