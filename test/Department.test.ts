import { Department } from '../src/models/Department';
import { Manager } from '../src/models/Manager';
import { Developer } from '../src/models/Developer';
import { Designer } from '../src/models/Designer';

describe("Department", function () {
    let department: Department;

    beforeEach(function () {
        department = new Department();
    });

    test("add manager is ok", function () {
        let manager = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });

        department.addManager(manager);
        expect(department.managers.includes(manager)).toBe(true);
    });

    test("remove manager is ok", function () {
        let manager = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });

        department.addManager(manager);
        department.removeManager(manager);
        expect(department.managers.includes(manager)).toBe(false);
    });

    test("can retrieve employees", function () {
        let manager = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });
        let employee = new Developer({ firstName: 'Sandor', lastName: 'Clegane', salary: 800, experience: 5 });

        manager.addToTeam(employee);
        department.addManager(manager);
        expect(department.employees.length).toEqual(1);
    });

    test("give salary to manager", function () {
        let manager = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });

        department.addManager(manager);

        spyOn(console, "log");
        department.giveSalary();

        expect(console.log).toHaveBeenCalledWith(`${manager.firstName} ${manager.lastName}: got salary: ${manager.salaryValue}`);
    });

    test("give salary to employee", function () {
        let manager = new Manager({ firstName: 'Cersei', lastName: 'Lannister', salary: 2000, experience: 5 });
        let employee = new Developer({ firstName: 'Sandor', lastName: 'Clegane', salary: 800, experience: 5 });

        manager.addToTeam(employee);
        department.addManager(manager);

        spyOn(console, "log");
        department.giveSalary();

        expect(console.log).toHaveBeenCalledWith(`${employee.firstName} ${employee.lastName}: got salary: ${employee.salaryValue}`);
    });
});

